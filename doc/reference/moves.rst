Move Functions
==============

.. automodule:: pygrates.moves

.. autofunction:: adjacent
.. autofunction:: children
.. autofunction:: parents
.. autofunction:: neighborhood
.. autofunction:: descendants
.. autofunction:: ancestors
.. autofunction:: explore
