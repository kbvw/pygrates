Abstract Base Classes
=====================

.. automodule:: pygrates.abc

.. autoclass:: GCoords
   :show-inheritance:
   :special-members:
   :inherited-members:
   :members:

.. autoclass:: DGCoords
   :show-inheritance:
   :special-members:
   :inherited-members:
   :members:
