Graph Functions
===============

.. automodule:: pygrates.graphs

.. autofunction:: inverse
.. autofunction:: compound
.. autofunction:: merge
.. autofunction:: degrees
.. autofunction:: subsources
.. autofunction:: subsinks
.. autofunction:: subgraph
