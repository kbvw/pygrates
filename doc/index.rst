.. include:: ../README.md
   :end-before: Documentation

Install
-------

The package is available from PyPI::

    pip install pygrates

For running the tests and building the documentation::

    pip install pygrates[test, doc]

.. include:: ../README.md
   :start-after: pip install pygrates
   :end-before: License

Development
-----------

The source code is hosted on Codeberg: https://codeberg.org/kbvw/pygrates.

License
-------

PyGrates is distributed under the MIT license:

.. literalinclude:: ../LICENSE
   :language: text

.. toctree::
   :maxdepth: 2
   :hidden: 

   tutorials/index.rst
   reference/index.rst
